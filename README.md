# Bachelor Project - Remy Moll

> Are the separations between distant background galaxies and foreground lenses random, or clustered?

## How to read
This document was redacted using obsidian, a relational markdown editor. It generates mostly vanilla markdown but also organises and labels for easier navigation. These features are not supported by gitlab's markdown renderer but the files can be navigated manually.

[https://help.obsidian.md/](https://help.obsidian.md/)
