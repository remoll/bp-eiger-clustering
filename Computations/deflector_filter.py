from pathlib import Path
import numpy as np
from astropy.table import Table
from astropy import units as u
from astropy import constants as const
import scipy
import matplotlib.pyplot as plt
import matplotlib.patches as patches


def add_magnitude_column(catalog):
    # lambdas = [115, 200, 356]

    for col in catalog.colnames:
        if "fnu" in col:
            new_col = col.replace("fnu", "mag")        
            flux = (catalog[col] * u.Jy).to(u.nJy)
            mag = np.abs(8.9 - 2.5 * np.log10(flux.value))
            # append column to catalog
            catalog[new_col] = mag
    
    return catalog

def ks_test(
    full_data,
    reduced_data,
    title,
    min_val: float = 0,
    max_val: float = None,
    ):
    ## Quick test that the kept redshifts follow the same distribution as the original
    max_val = np.max(full_data) if max_val is None else max_val
    bin_edges = np.linspace(min_val, max_val, 20)
    bin, bin_edges = np.histogram(full_data, bins=bin_edges, density=True)
    reduced_bin, reduced_bin_edges = np.histogram(reduced_data, bins=bin_edges, density=True)
    stats, p = scipy.stats.kstest(bin, reduced_bin)
    print(f"KS test for z-distribution: p={p:.2f} {stats=}")
    plt.figure()
    plt.plot(bin_edges[:-1], bin, "o", label="Initial population")
    plt.plot(reduced_bin_edges[:-1], reduced_bin, "x", label="Reduced population")
    plt.title(f"KS test: {title}")
    plt.ylabel("Probability density")
    plt.legend()
    plt.show()


def simulated_deflector_selection(
        magnifications: np.array,
        source_data: Table,
        deflector_data: Table,
        angular_separation_range: list|tuple,
        minimum_mu: float = 2,
        minimum_mu_threshold: float = 0.5,
        show_plots: bool = False,
    ) -> Table:
    """
    Discards deflectors from the deflectors catalog (JAGUAR simulated) that do not produce a magnification of at least minimum_mu (*) for a source with angular separation between angular_separation_range.
    (*) The minimum_mu_threshold fraction of sources must have a magnification > minimum_mu.
    :param magnifications: magnification array of shape (n_deflector, m_source)
    :param source_data: Table with columns (z, sep)
    :param deflector_data: Table with columns (z, m, r, sigma)
    :param angular_separation_range: angular separation range in arcsec [min, max] of the sources
    :param minimum_mu: minimum magnification required for a deflector to be kept
    :param minimum_mu_threshold: fraction of sources that must have a magnification > minimum_mu for a given deflector to be kept
    :param show_plots: whether to show plots
    :return: reduced catalog with an additional column "NRC_F356W_mag" containing the magnitude of the source in the F356W filter (as used in the EIGER data)
    """
    # find indices of sources that lie within angular_separation_range
    lower, upper = angular_separation_range # implicit unpacking
    row_keep_indices = np.where((source_data["sep"].value > lower) & (source_data["sep"].value < upper))[0]
    print(f"Considering {row_keep_indices.size}/{len(source_data)} source configurations for magnitude threshold.")
    mag_reduced = magnifications[row_keep_indices, ...]

    # The column indices are the deflector indices, of which we only want to keep the ones satisfying the conditions
    # Now we're working with an already reduced array of magnifications of deflector/source tuples delimited by angular_separation_range
    mag_threshold_indices = mag_reduced > minimum_mu
    col_sum = np.sum(mag_threshold_indices, axis=0)

    minimum_rows = mag_reduced.shape[0] * minimum_mu_threshold # minimum number of rows that must have a magnification > minimum_mu
    col_keep_indices = col_sum > minimum_rows
    reduced_deflector_data = deflector_data[col_keep_indices]
    print(f"Keeping {len(reduced_deflector_data)} deflectors out of {col_keep_indices.size}.")


    ### Show some key information about the selected subset of deflectors
    if show_plots:
        plt.figure()
        plt.semilogy(deflector_data["z"], deflector_data["sigma"], "o", label="Initial deflector population")
        plt.semilogy(reduced_deflector_data["z"], reduced_deflector_data["sigma"], "x", label="Reduced deflector population")
        plt.xlabel("Deflector redshift")
        plt.ylabel("Deflector sigma")
        plt.legend()
        plt.show()

        ks_test(deflector_data["z"], reduced_deflector_data["z"], "z-distribution")
        ks_test(deflector_data["sigma"], reduced_deflector_data["sigma"], "sigma-distribution")

    reduced_catalog = deflector_data[col_keep_indices]
    reduced_catalog = add_magnitude_column(reduced_catalog)
    # path = f"../Data/JAGUAR/potential_deflectors_sep{lower}-{upper}_mu{minimum_mu}_mu_thres{minimum_mu_threshold}.fits"
    # reduced_catalog.write(path, overwrite=True)
    return reduced_catalog



def cc_boundaries(method, color_x, color_y):
    if method == "minmax":
        # select the deflectors within the square locus spanned by the potential deflectors
        x_upper = color_x.max(initial = 0)
        x_lower = color_x.min(initial = 0)
        y_upper = color_y.max(initial = 0)
        y_lower = color_y.min(initial = 0)
        return [x_lower, x_upper], [y_lower, y_upper]

    elif method == "normal":
        # select the deflectors within the normally distributed locus spanned by the potential deflectors
        x_lower = color_x.mean() - 2 * color_x.std()
        x_upper = color_x.mean() + 2 * color_x.std()
        y_lower = color_y.mean() - 2 * color_y.std()
        y_upper = color_y.mean() + 2 * color_y.std()
        return [x_lower, x_upper], [y_lower, y_upper]

    else:
        raise ValueError(f"Method {method} not recognized.")

        
def actual_deflector_selection(
        full_science_catalog_path: str,
        target_dir: Path,
        # method: str = "minmax",
        method: str = "normal",
        potential_deflectors: Table = None,
        show_plots: bool = False
    ):
    """
    Selects actual deflectors from actual JWST data ccording to the potential deflectors table.
    If potential_deflectors is provided we compute freshly. Otherwise we try to load the data from the target_dir. 
    """
    full_science_objects = Table.read(full_science_catalog_path)
    deflectors_fname = f"selected_deflectors_{method}.fits"
    deflectors_file_location = target_dir / deflectors_fname

    if potential_deflectors is None:
        potential_deflectors = Table.read(deflectors_file_location)
        # raises FileNotFoundError if the file does not exist
        return potential_deflectors
    
    print("Applying filtering to full science catalog to obtain deflector selection")

    ## Filtering according to simple magnitude criteria
    m_keep_indices = np.nonzero(
        (full_science_objects["MAG_AUTO_F356W_apcor"] < potential_deflectors["NRC_F356W_mag"].max(initial=0))
        # MAGNITUDES ARE NEGATIVE
        # (full_science_objects["MAG_AUTO_F356W_apcor"] < 3 * potential_deflectors["NRC_F356W_mag"].max(initial=0))
    )

    ## Filtering according to Color-Color criteria
    # in the color-color space, only keep the J0100 deflectors that are within the square locus of the potential deflectors
    cx_simulated = potential_deflectors["NRC_F115W_mag"] - potential_deflectors["NRC_F200W_mag"]
    cy_simulated = potential_deflectors["NRC_F200W_mag"] - potential_deflectors["NRC_F356W_mag"]
    weight_simulated = potential_deflectors["m"]

    cx_j0100 = full_science_objects["MAG_AUTO_F115W_apcor"] - full_science_objects["MAG_AUTO_F200W_apcor"]
    cy_j0100 = full_science_objects["MAG_AUTO_F200W_apcor"] - full_science_objects["MAG_AUTO_F356W_apcor"]


    x_lim, y_lim = cc_boundaries(method, cx_simulated, cy_simulated)

        
    cc_keep_indices = np.nonzero(
        (cx_j0100 > x_lim[0]) & \
        (cx_j0100 < x_lim[1]) & \
        (cy_j0100 > y_lim[0]) & \
        (cy_j0100 < y_lim[1])
        )

    row_keep_indices = np.intersect1d(cc_keep_indices, m_keep_indices)
    actual_deflectors = full_science_objects[row_keep_indices]

    potential_interlopers = np.sum( # these are the objects lying outside the range defined by the methods boundaries
        (cx_simulated < x_lim[0]) | \
        (cx_simulated > x_lim[1]) | \
        (cy_simulated < y_lim[0]) | \
        (cy_simulated > y_lim[1])
    )

    print(f"Method {method} generates {potential_interlopers} interlopers -> out of {len(potential_deflectors)} only {len(potential_deflectors) - potential_interlopers} are retained")
    try:
        deflectors_minmax = Table.read(target_dir / f"selected_deflectors_minmax.fits")
        print(f"Minmax method generates {len(deflectors_minmax)}/{len(full_science_objects)} deflectors")
        print(f"{method} method generates {len(actual_deflectors)}/{len(full_science_objects)} deflectors")
    except FileNotFoundError:
        print("Minmax method not found. Skipping comparison.")

    actual_deflectors.write(deflectors_file_location, overwrite=True)


    ### Plotting
    if show_plots:
        plt.figure()
        plt.title("Color distribution of JAGUAR data")
        plt.hist(cx_simulated, bins=20, label="B-V deflectors", density=True, alpha=0.5)
        plt.hist(cy_simulated, bins=20, label="V-G deflectors", density=True, alpha=0.5)
        plt.show()

        plt.figure()
        plt.title("Magnitude distribution of JAGUAR and EIGER data")
        plt.hist(full_science_objects["MAG_AUTO_F356W_apcor"], bins=200, label="M (J0100 deflectors)", density=True, alpha=0.5)
        plt.hist(potential_deflectors["NRC_F356W_mag"], bins=20, label="M (JAGUAR)", density=True, alpha=0.5)
        plt.xlim(15, 35)
        plt.legend()
        plt.show()


        ## Make this plot extra-fancy for the report:
        method_labels = {
            "minmax": "$min$-$max$-locus of simulated deflector color range",
            "normal": "$1.5\\sigma$-locus of normally distributed color range"
        }

        plt.figure()
        plt.scatter(cx_j0100, cy_j0100, c="grey", alpha=0.5, s=1, label="$J0100$ deflectors")
        plt.scatter(cx_j0100[row_keep_indices], cy_j0100[row_keep_indices], marker="*", color="none", edgecolor="black", label="$J0100$ deflectors (magnitude and color selected)")
        size = (weight_simulated.value * 10e-9 * 0.4)**(0.8) * 0.5
        sc = plt.scatter(cx_simulated, cy_simulated, s=size, label="Simulated potential deflectors", c=potential_deflectors["z"], cmap="coolwarm")
        
        # This is the rectangle for the current method
        dims = [x_lim[1] - x_lim[0], y_lim[1] - y_lim[0]]
        rect = patches.Rectangle((x_lim[0], y_lim[0]), dims[0], dims[1], linewidth=1, edgecolor='black', facecolor='none', label=method_labels[method])
        plt.gca().add_patch(rect)

        # This is the rectangle for the normal method
        x_lim_normal, y_lim_normal = cc_boundaries("normal", cx_simulated, cy_simulated)
        dims_normal = [x_lim_normal[1] - x_lim_normal[0], y_lim_normal[1] - y_lim_normal[0]]
        rect_normal = patches.Rectangle((x_lim_normal[0], y_lim_normal[0]), dims_normal[0], dims_normal[1], linestyle="dashed", linewidth=1, edgecolor='black', facecolor='none', label=method_labels["normal"])
        plt.gca().add_patch(rect_normal)

        plt.colorbar(sc, label="Deflector Redshift")
        plt.xlabel("$M_{115} - M_{200}$")
        plt.ylabel("$M_{200} - M_{356}$")
        plt.xlim(-3, 2)
        plt.ylim(-2, 2)
        plt.legend()
        plt.show()




        ks_test(cx_j0100, cx_j0100[row_keep_indices], "b-v distribution", -3, 3)
        ks_test(cy_j0100, cy_j0100[row_keep_indices], "v-r distribution", -3, 3)

        mag_j0100 = full_science_objects["MAG_AUTO_F356W_apcor"]
        ks_test(mag_j0100, mag_j0100[row_keep_indices], "magnitude distribution", 15, 35)

        # z_j0100 = full_science_objects["z"]
        # ks_test(z_j0100, z_j0100[row_keep_indices], "redshift distribution")
        
        # sigma_j0100 = full_science_objects["sigma"]
        # ks_test(sigma_j0100, sigma_j0100[row_keep_indices], "velocity dispersion distribution")

    return actual_deflectors



def load_or_compute_actual_deflectors(
    target_dir,
    full_science_catalog_path,
    magnifications,
    deflectors,
    sources,
    angular_separation_range,
    minimum_mu,
    minimum_mu_threshold,
    deflector_selection_method,
    show_plots: str = None,
    return_potential_deflectors = False,
    ):
    if not return_potential_deflectors:
        try: # to load from cache, without computing potential deflectors
            actual_deflectors = actual_deflector_selection(
                target_dir = target_dir,
                full_science_catalog_path = full_science_catalog_path,
                method = deflector_selection_method,
                show_plots = False
                # plots won't ever be shown in this configuration
            )
            return actual_deflectors
        except FileNotFoundError:
            pass


    print("No cached data found, computing simulated deflectors...")
    potential_deflectors = simulated_deflector_selection(
        magnifications = magnifications,
        deflector_data = deflectors,
        source_data = sources,
        angular_separation_range = angular_separation_range,
        minimum_mu = minimum_mu,
        minimum_mu_threshold = minimum_mu_threshold,
        show_plots = True if show_plots == "simulated" else False
    )

    actual_deflectors = actual_deflector_selection(
        target_dir = target_dir,
        full_science_catalog_path = full_science_catalog_path,
        potential_deflectors = potential_deflectors,
        method = deflector_selection_method,
        show_plots = True if show_plots == "actual" else False
    )

    if return_potential_deflectors:
        return actual_deflectors, potential_deflectors
    else:
        return actual_deflectors