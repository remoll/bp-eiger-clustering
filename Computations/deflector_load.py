from pathlib import Path
from astropy import units as u
from astropy.table import Table
import matplotlib.pyplot as plt
import compute_magnifications



def load_simulated_from_path(path, show_plots=False) -> Table:
    """Loads the fits catalog of the JAGUAR simulated to serve as the deflectors"""
    path = Path(path)
    catalog = Table.read(path)
    initial_size = len(catalog)
    old_z = catalog['redshift']

    # Apply obvious filtering (mass + redshift requirements imposed by JWST later on)
    catalog = catalog[catalog['redshift'] > 0.1]
    catalog = catalog[catalog['redshift'] < 5]
    catalog = catalog[catalog['mStar'] > 10]

    print(f"Keeping {len(catalog)}/{initial_size} galaxies from {path.name}")

    z = catalog['redshift']
    m = 10**catalog['mStar'] * u.Msun # catalog['mStar'] is in log10(Msun)
    r = catalog['Re_circ'] * u.kpc # half-light radius https://astronomy.swin.edu.au/cosmos/h/Half-light+Radius
    sigma = compute_magnifications.compute_sigma(m, r).to(u.km/u.s)

    if show_plots:
        ## Verify sensible selector data
        plt.figure()
        plt.hist(z, bins=30, density=True, label="Retained deflectors")
        plt.hist(old_z, bins=30, density=True, alpha=0.5, label="Full catalog")
        plt.xlabel('redshift')
        plt.ylabel('count')
        plt.legend()
        plt.show()

        plt.figure()
        plt.plot(sigma.value, m, 'o')
        plt.xlabel(f'velocity dispersion [{sigma.unit}]')
        plt.ylabel(f'stellar mass [{m.unit}]')
        plt.show()

        plt.figure()
        plt.title("Power law constraining of velocity dispersion")
        plt.semilogy(z, sigma.value, 'o')
        lower_pow = 1.2**z * 100
        upper_pow = 3**z * 100

        plt.semilogy(z, lower_pow, 'r--')
        plt.semilogy(z[:500], upper_pow[:500], 'r--', label='$1.2^z$ and $3^z$')
        plt.legend()
        plt.xlabel(f'redshift')
        plt.ylabel(f'velocity dispersion [{sigma.unit}]')
        plt.show()

    table = Table(
        data=[z, m, r, sigma],
        names=['z', 'm', 'r', 'sigma'],
    )


    # Also add flux data required later to apply thresholds to the real data
    for col in catalog.colnames:
        if col.startswith('NRC_F'):
            table[col] = catalog[col]

    return table