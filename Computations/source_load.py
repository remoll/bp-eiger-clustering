from astropy.table import Table
from astropy.wcs import WCS
from astropy.io import fits


def load_sources(
    source_catalog_path: str,
    source_frame_path: str,
    special_redshift: float = None
    ):
    """Load the sources from the source file. If a redshift is given, only sources around that redshift will be returned."""
    sources = Table.read(source_catalog_path)

    if "x" not in sources.colnames or "y" not in sources.colnames:
        hdu = fits.open(source_frame_path)
        wcs = WCS(hdu['SCI'].header)

        x, y = wcs.all_world2pix(sources["RA_MANUAL"], sources["DEC_MANUAL"], 0)
        sources["x"] = x
        sources["y"] = y


    if special_redshift is not None:
        offset = 0.1
        sources = sources[(sources["redshift"] > special_redshift - offset) & (sources["redshift"] < special_redshift + offset)]

    return sources


