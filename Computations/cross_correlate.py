import subprocess
import uuid

import numpy as np
from astropy.table import Table
from astropy import units as u
from astropy.wcs import WCS, FITSFixedWarning
from astropy.io import fits

import matplotlib.pyplot as plt
from pathlib import Path


import warnings
warnings.filterwarnings('ignore', category=FITSFixedWarning, append=True)

CC_BASE_FNAME = "cross_correlated__"

def get_coords_from_table(table: Table):
    """Return the names of the coordinates in the table."""
    names = table.colnames
    if "x" in names and "y" in names:
        return ["x", "y"]
    elif "X" in names and "Y" in names:
        return ["X", "Y"]
    # otherwise the x,y coords are named with some additional string
    for col in names:
        if "x_" in col or "X_" in col or "_x" in col or "_X" in col:
            return [col, col.replace("x", "y").replace("X", "Y")]

    raise ValueError("No coordinates found in table.")

def stilts_tmatch(
    table1: Table,
    table2: Table,
    max_sep: int,
    output_file: str = None,
    suppress_output: bool = True,
):
    """
    Use stilts to match two tables based on pixel coordinates.
    Creates a new table with the distances between all permutations of the objects in the two tables.
    """
    t1_path = Path(f"/tmp/{uuid.uuid4()}.fits")
    t2_path = Path(f"/tmp/{uuid.uuid4()}.fits")
    table1.write(t1_path, overwrite=True)
    table2.write(t2_path, overwrite=True)

    if output_file is None:
        del_out = True
        output_file = Path(f"/tmp/{uuid.uuid4()}.fits")
    else:
        print(f"Writing output to {output_file}")
        del_out = False

    t1_coords = get_coords_from_table(table1)
    t2_coords = get_coords_from_table(table2)
    if not suppress_output:
        print(f"Matching using coordinates {t1_coords} and {t2_coords}.")
    subprocess.check_call(
        f"topcat -stilts tmatch2 join=1and2 find=all params={max_sep} matcher=2d "
        f"in1={t1_path} values1='{t1_coords[0]} {t1_coords[1]}' "
        f"in2={t2_path} values2='{t2_coords[0]} {t2_coords[1]}' "
        f"out={output_file}",
        stdout=subprocess.DEVNULL if suppress_output else None,
        stderr=subprocess.DEVNULL if suppress_output else None,
        shell=True
    )
    result = Table.read(output_file)

    t1_path.unlink()
    t2_path.unlink()
    if del_out:
        output_file.unlink()

    return result



class CrossCorrelationCalculator:
    def __init__(
            self,
            target_dir: Path,
            sources: Table,
            source_frame_path: Path,
            randoms_path: Path,
            deflector_selection_method: str,
            binning_method: str,
            nbins: int,
            binning_max_pixels: int,
            show_plots: bool = False
        ):
        self.target_dir = target_dir
        self.show_plots = show_plots
        self.sources = sources
        self.source_frame_path = source_frame_path
        self.binning_max_pixels = binning_max_pixels
        self.randoms_path = randoms_path

        self.binning_method = binning_method

        if binning_method == "lin":
            self.bins = np.linspace(0, binning_max_pixels, num=nbins)
        elif binning_method == "log":
            split = int(0.2 * binning_max_pixels) # keep linear bins for the first 20% of the range (too many bins otherwise)
            self.bins = np.concatenate((
                np.linspace(1, split, num = 3, endpoint=False),
                np.geomspace(split, binning_max_pixels, num=nbins-3)
            ))
            # self.bins = np.geomspace(0.1, binning_max_pixels, num=nbins)
        elif binning_method == "same_fill":
            # bins will have to be filled each with the same number of objects
            self.bins = np.zeros(nbins)
        else:
            raise ValueError(f"Unknown binning method: {binning_method}")

        self.o3_file_target = self.target_dir / f"{CC_BASE_FNAME}deflectors_{deflector_selection_method}_sources.fits"
        self.randoms_file_target = self.target_dir / f"{CC_BASE_FNAME}deflectors_{deflector_selection_method}_randoms.fits"
        self.bin_path_base = f"{CC_BASE_FNAME}hists__deflectors_{deflector_selection_method}_randoms__{self.binning_method}__n" # .npy


    def match_with_o3_sources(self, deflectors: Table):
        """Use stilts skymatch to find the distance between each deflector and each O3 source.
        """
        try: # if the file already exists, just read it
            matches = Table.read(self.o3_file_target)
        except FileNotFoundError:
            matches = stilts_tmatch(deflectors, self.sources, self.binning_max_pixels, suppress_output = True, output_file = self.o3_file_target)

        if np.sum(self.bins) == 0:
            # fill bins with the same number of objects
            if len(matches) == 0:
                bins = np.array([0])
            else:
                bins = np.quantile(matches["Separation"], np.linspace(0, 1, self.bins.size))
            self.bins = bins

        hist, edges = np.histogram(matches["Separation"], bins=self.bins, density=False)
        if self.show_plots:
            plt.figure()
            plt.bar(edges[:-1], hist, width=edges[1]-edges[0], alpha=0.5)
            plt.xlabel("Distance between deflector and source (arcsec)")
            plt.ylabel("Matches found (density)")
            plt.show()

        return hist

    
    def match_with_random(
        self,
        deflectors: Table,
        randoms: Table,
        n_draws: int,
    ):
        """
        Perform multiple matches of the actual deflectors with randomly laid out sources.
        """
        try:
            matches = Table.read(self.randoms_file_target)
        except FileNotFoundError:
            # drop uesless columns
            deflectors = deflectors["X_IMAGE_det", "Y_IMAGE_det"]
            randoms_id = np.arange(len(randoms), dtype=int)
            randoms = randoms["x", "y"]
            randoms["id"] = randoms_id

            print("Matching deflectors with 'randoms' Table...")
            matches = stilts_tmatch(
                randoms,
                deflectors,
                self.binning_max_pixels,
                output_file = self.randoms_file_target,
            )
            # This only generates the matches lying within the max separation, but we want all of them.
            # Will have to account for this afterwards.
        
        sample_size = len(self.sources)
        deflector_size = len(deflectors)
        random_size = len(randoms)
        hists = np.zeros((n_draws, self.bins.size - 1))

        # normally for every random out of the catalog there will be exactly M=len(deflectors) matches. But the catalog only contains the matches within the max separation.
        # so we take all the matches for a subset of the randoms first
        # Then we have a look at their length which will be less than sample_size * M
        # We then fill up with values of max_sep since all the matches that are not in the catalog are at >= max_sep
        
        no_match_value = 2 * self.binning_max_pixels

        for i in range(n_draws):
            rng = np.random.default_rng()
            randoms_subset_selection = rng.choice(random_size, size=sample_size, replace=False)
            # pick unique randoms out of the catalog
            unique_matches, matches_indices_count = np.unique(matches["id"], return_counts=True)
            unique_matches_global_idx = np.cumsum(matches_indices_count)
            # not all randoms_subset_selection_ids are in the catalog. We won't be able to slice the table with them.
            # pick the intersection of indices that actually are in the catalog
            _, idx_intersect_randoms_subset_selection, idx_intersect_unique_matches = np.intersect1d(randoms_subset_selection, unique_matches, assume_unique=True, return_indices=True)
            
            # for the draws we actually take out of the catalog we need to know how many matches there are for each of them
            matches_count = matches_indices_count[idx_intersect_unique_matches]

            # allocate memory for the indices of the matches we want to keep
            matches_keep_indices = np.zeros(matches_count.sum(), dtype=int)
            # fill the array with the indices of the matches we want to keep
            idx = 0
            for j in range(idx_intersect_unique_matches.size):
                count = matches_count[j]
                start_index = unique_matches_global_idx[j]
                matches_keep_indices[idx : idx + count] = np.arange(0, count, dtype=int) + start_index
                idx += count
            # matches_keep_indices = np.zeros(loc_in_all_matches.size * matches_count.size, dtype=int)
            # idx = 0
            # for j in range(loc_in_all_matches.size):
            #     count = matches_count[j]
            #     matches_keep_indices[idx : idx + count] = np.arange(0, count, dtype=int) + loc_in_all_matches[j]
            #     idx += count
            
            # assert np.max(matches_keep_indices, initial=0) <= len(matches)
            
            random_subset_sep = matches["Separation"][matches_keep_indices]
            full_match_sep = np.concatenate((
                random_subset_sep,
                no_match_value * np.ones(sample_size * deflector_size - random_subset_sep.size)
            ))
            hist, _ = np.histogram(full_match_sep, bins=self.bins, density=False)
            hists[i, ...] = hist

        return hists


    def compute_random_separation_expectation(
            self,
            deflectors: Table,
            n_draws: int = 1000,
        ):
        """Compute the expected distribution of distances between deflectors and random sources."""
        random_objects = Table.read(self.randoms_path)


        for p in self.target_dir.rglob(self.bin_path_base + "*.npy"):
            if str(n_draws) in p.name:
                hists = np.load(p)
                break
            
        else: # if we didn't find the file
            print(f"Computing random separation expectation for {n_draws} draws...")
            hists = self.match_with_random(
                deflectors = deflectors,
                randoms = random_objects,
                n_draws = n_draws,
            )
            np.save(
                self.target_dir / (self.bin_path_base + f"{n_draws}.npy"),
                hists
            )

        mean_hist = np.mean(hists, axis=0)
        std_hist = np.std(hists, axis=0)

        if self.show_plots:
            plt.figure()
            width = self.bins[1] - self.bins[0]
            plt.bar(self.bins[:-1], mean_hist, width=width, yerr=std_hist)
            plt.xlabel("Distance between deflector and random (arcsec)")
            plt.ylabel("Matches found (density)")
            plt.show()
        
        return mean_hist, std_hist


    @property
    def bins_as_arcsec(self) -> np.array(u.arcsec):
        """Returns the bins of this CCC instance in arcseconds."""
        hdu = fits.open(self.source_frame_path)
        wcs = WCS(hdu['SCI'].header)
        return (self.bins * wcs.pixel_scale_matrix[0][0] * u.degree).to(u.arcsec)
