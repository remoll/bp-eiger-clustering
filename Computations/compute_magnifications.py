import numpy as np
from astropy import units as u
from astropy import constants as const
from astropy.cosmology import Planck18 as cosmo
import os


# Astophysical relations
# all functions accept arrays and return arrays
def compute_sigma(mass, radius): # by virial theorem
    return np.sqrt(const.G * mass / radius) #.to(u.km/u.s)

def compute_einstein_radius(sigma, dls, ds):
    return (4 * np.pi * (sigma / const.c)**2 * (dls / ds)).to(u.arcsec, equivalencies=u.dimensionless_angles())

def compute_magnification(einstein_radius, separation):
    sep = np.abs(separation.value) * separation.unit
    return sep / (sep - einstein_radius)


## With 2D phase space:

def compute_einstein_radius_2d(sigma, dls, ds):
    """
    Sigma: (1, n), dls: (n, m), ds: (1, m)
    returns einstein radius: (n, m)
    """
    result = np.zeros_like(dls.value)
    for idx, d in np.ndenumerate(dls):
        # d somehow lost its unit
        d *= u.Mpc
        s = sigma[idx[1]] * sigma.unit
        result[idx] = (4 * np.pi * (s / const.c)**2 * (d / ds[idx[0]])) # dimensionless

    return result * u.rad


def compute_magnification_2d(einstein_radius, separation):
    """Einstein radius: (n, m), separation: (n, 1)"""
    # np.abs(separation) / (np.abs(separation) - einstein_radius[...,k])
    return np.array([compute_magnification(einstein_radius[..., k], separation).to(u.dimensionless_unscaled) for k in range(einstein_radius.shape[1])])


def compute_magnifications(source_data, deflector_data, compute_new=False):
    """ Compute the magnification for all possible source-lens combinations
    :param source_data: Table with columns (z, sep)
    :param deflector_data: Table with columns (z, m, r, sigma)
    :param compute_new: bool, if True, compute magnifications from scratch. If False, load from file (if file exists)
    :return: np.ndarray with shape: (len(source_data), len(deflector_data))
    """
    if not compute_new:
        if os.path.isfile('magnifications.npy'):
            magnifications = np.load('magnifications.npy')
            if magnifications.shape == (len(source_data), len(deflector_data)):
                return magnifications
        
    print("Computing fresh magnifications")

    width = len(source_data)
    height = len(deflector_data)
    z_deflector_inflated = np.tile(deflector_data["z"].value.data, (width, 1))
    sigma_deflector_inflated = np.tile(deflector_data["sigma"].value.data, (width, 1)) * deflector_data["sigma"].unit
    z_source_inflated = np.tile(source_data["z"].value.data, (height, 1)).T
    sep_source_inflated = np.tile(source_data["sep"].value.data, (height, 1)).T * source_data["sep"].unit

    ds = cosmo.angular_diameter_distance(z_source_inflated)
    dls = cosmo.angular_diameter_distance_z1z2(z_deflector_inflated, z_source_inflated)

    einstein_radius = compute_einstein_radius(sigma_deflector_inflated, dls, ds)
    magnifications = compute_magnification(einstein_radius, sep_source_inflated).value # is dimensionless
    np.save('magnifications.npy', magnifications)

    return magnifications


def normalize_magnifications(magnifications):
    """Removes sharp and negative mu values for more homogenous plotting"""
    print(f"Initial mu values: max {np.max(magnifications):.2f}, min {np.min(magnifications)}, mean {np.mean(magnifications):.2f}, std {np.std(magnifications):.2f}")
    print(f"Correcting negative lensing for {np.sum(magnifications < 0)} pairs ({(np.sum(magnifications < 0) / magnifications.size * 100):.4f} %)")
    print(f"Correcting strong lensing for {np.sum(magnifications > 15)} pairs ({(np.sum(magnifications > 15) / magnifications.size * 100):.4f} %)")

    magnifications = np.abs(magnifications)
    magnifications[magnifications > 15] = 15

    print(f"Final mu values: max {np.max(magnifications):.2f}, min {np.min(magnifications)}, mean {np.mean(magnifications):.2f}, std {np.std(magnifications):.2f}")
    return magnifications


def inflate_magnifications(magnifications):
    """
    By construction magnifications is in a 2D array but one axis contains (separation x redshift)
    Separate it back into a 3D structure
    """
    dim_1_and_2 = int(np.sqrt(magnifications.shape[0]))
    return magnifications.reshape(dim_1_and_2, dim_1_and_2, magnifications.shape[1])